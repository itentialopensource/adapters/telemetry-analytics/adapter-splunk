
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_19:51PM

See merge request itentialopensource/adapters/adapter-splunk!15

---

## 0.5.3 [09-18-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-splunk!13

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_18:00PM

See merge request itentialopensource/adapters/adapter-splunk!12

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_19:13PM

See merge request itentialopensource/adapters/adapter-splunk!11

---

## 0.5.0 [05-20-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-splunk!10

---

## 0.4.6 [03-27-2024]

* Changes made at 2024.03.27_13:10PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-splunk!9

---

## 0.4.5 [03-13-2024]

* Changes made at 2024.03.12_10:56AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-splunk!7

---

## 0.4.4 [02-27-2024]

* Changes made at 2024.02.27_11:30AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-splunk!6

---

## 0.4.3 [12-25-2023]

* update metadata

See merge request itentialopensource/adapters/telemetry-analytics/adapter-splunk!5

---

## 0.4.2 [12-20-2023]

* Add v2 endpoints and add tasks with query data

See merge request itentialopensource/adapters/telemetry-analytics/adapter-splunk!4

---

## 0.4.1 [12-01-2023]

* Update request data types to URLENCODE for post calls

See merge request itentialopensource/adapters/telemetry-analytics/adapter-splunk!3

---

## 0.4.0 [11-07-2023]

* More migration changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-splunk!2

---

## 0.3.0 [10-19-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-splunk!1

---

## 0.2.0 [09-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-splunk!1

---

## 0.1.1 [09-30-2022]

* Bug fixes and performance improvements

See commit d579c57

---
