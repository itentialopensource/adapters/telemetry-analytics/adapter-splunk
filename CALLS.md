## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Splunk. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Splunk.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>

### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Splunk. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getDataCommandsByName(name, callback)</td>
    <td style="padding:15px">getDataCommandsByName</td>
    <td style="padding:15px">{base_path}/{version}/services/data/commands/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSavedSearches(earliestTime10202022, latestTime, listDefaultActionArgs, addOrphanField, callback)</td>
    <td style="padding:15px">getSavedSearches</td>
    <td style="padding:15px">{base_path}/{version}/services/saved/searches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSavedSearchesByName(name, body, callback)</td>
    <td style="padding:15px">deleteSavedSearchesByName</td>
    <td style="padding:15px">{base_path}/{version}/services/admin/search/saved/searches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSavedSearchesByName(name, earliestTime10202022, latestTime, listDefaultActionArgs, addOrphanField, callback)</td>
    <td style="padding:15px">getSavedSearchesByName</td>
    <td style="padding:15px">{base_path}/{version}/services/admin/search/saved/searches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSavedSearches(name, body, callback)</td>
    <td style="padding:15px">postSavedSearches</td>
    <td style="padding:15px">{base_path}/{version}/services/admin/search/saved/searches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSavedSearchesAcknowledge(name, body, callback)</td>
    <td style="padding:15px">postSavedSearchesAcknowledge</td>
    <td style="padding:15px">{base_path}/{version}/services/admin/search/saved/searches/{pathv1}/acknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSavedSearchesDispatchByName(name, body, callback)</td>
    <td style="padding:15px">postSavedSearchesDispatchByName</td>
    <td style="padding:15px">{base_path}/{version}/services/saved/searches/{pathv1}/dispatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSavedSearchesHistoryByName(name, savedsearch, callback)</td>
    <td style="padding:15px">getSavedSearchesHistoryByName</td>
    <td style="padding:15px">{base_path}/{version}/services/saved/searches/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSavedSearchesReschedule(name, body, callback)</td>
    <td style="padding:15px">postSavedSearchesReschedule</td>
    <td style="padding:15px">{base_path}/{version}/services/saved/searches/{pathv1}/reschedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSavedSearchesScheduledItemsByName(name, earliestTime, latestTime, callback)</td>
    <td style="padding:15px">getSavedSearchesScheduledItemsByName</td>
    <td style="padding:15px">{base_path}/{version}/services/saved/searches/{pathv1}/scheduled_times?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSavedSearchesSupresssByName(name, expiration, callback)</td>
    <td style="padding:15px">getSavedSearchesSupresssByName</td>
    <td style="padding:15px">{base_path}/{version}/services/saved/searches/{pathv1}/suppress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduledViews(count, f, offset, search, sortDir, sortKey, sortMode, summarize, callback)</td>
    <td style="padding:15px">getScheduledViews</td>
    <td style="padding:15px">{base_path}/{version}/services/scheduled/views?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScheduledViewByName(name, body, callback)</td>
    <td style="padding:15px">deleteScheduledViewByName</td>
    <td style="padding:15px">{base_path}/{version}/services/scheduled/views/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduledViewByName(name, callback)</td>
    <td style="padding:15px">getScheduledViewByName</td>
    <td style="padding:15px">{base_path}/{version}/services/scheduled/views/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postScheduledViewByName(name, body, callback)</td>
    <td style="padding:15px">postScheduledViewByName</td>
    <td style="padding:15px">{base_path}/{version}/services/scheduled/views/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postScheduledViewDispatch(name, body, callback)</td>
    <td style="padding:15px">postScheduledViewDispatch</td>
    <td style="padding:15px">{base_path}/{version}/services/scheduled/views/{pathv1}/dispatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduledViewHistory(name, callback)</td>
    <td style="padding:15px">getScheduledViewHistory</td>
    <td style="padding:15px">{base_path}/{version}/services/scheduled/views/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postScheduledViewHistory(name, body, callback)</td>
    <td style="padding:15px">postScheduledViewHistory</td>
    <td style="padding:15px">{base_path}/{version}/services/scheduled/views/{pathv1}/reschedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduledViewScheduledTimes(name, earliestTime, latestTime, callback)</td>
    <td style="padding:15px">getScheduledViewScheduledTimes</td>
    <td style="padding:15px">{base_path}/{version}/services/scheduled/views/{pathv1}/scheduled_times?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchJobs(count, f, offset, search, sortDir, sortKey, sortMode, summarize, callback)</td>
    <td style="padding:15px">getSearchJobs</td>
    <td style="padding:15px">{base_path}/{version}/services/search/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSearchJobs(body, callback)</td>
    <td style="padding:15px">postSearchJobs</td>
    <td style="padding:15px">{base_path}/{version}/services/search/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchJobsExport(callback)</td>
    <td style="padding:15px">getSearchJobsExport</td>
    <td style="padding:15px">{base_path}/{version}/services/search/jobs/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSearchJobsExport(body, callback)</td>
    <td style="padding:15px">postSearchJobsExport</td>
    <td style="padding:15px">v1:{base_path}/{version}/services/search/jobs/export?{query} <br /> v2:{base_path}/{version}/services/search/v2/jobs/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSearchJobsById(searchId, body, callback)</td>
    <td style="padding:15px">deleteSearchJobsById</td>
    <td style="padding:15px">{base_path}/{version}/services/search/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchJobsById(searchId, callback)</td>
    <td style="padding:15px">getSearchJobsById</td>
    <td style="padding:15px">{base_path}/{version}/services/search/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSearchJobsById(searchId, body, callback)</td>
    <td style="padding:15px">postSearchJobsById</td>
    <td style="padding:15px">{base_path}/{version}/services/search/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSearchJobsByIdControl(searchId, body, callback)</td>
    <td style="padding:15px">postSearchJobsByIdControl</td>
    <td style="padding:15px">{base_path}/{version}/services/search/jobs/{pathv1}/control?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchJobsByIdEvents(searchId, count, maxLines, outputMode, truncationMode, callback)</td>
    <td style="padding:15px">getSearchJobsByIdEvents</td>
    <td style="padding:15px">v1:{base_path}/{version}/services/search/jobs/{pathv1}/events?{query} <br /> v2:{base_path}/services/search/v2/jobs/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchJobsByIdResultsWithOptions(searchId, queryData, callback)</td>
    <td style="padding:15px">getSearchJobsByIdResults with query data</td>
    <td style="padding:15px">v1:{base_path}/{version}/services/search/jobs/{pathv1}/results?{query} <br /> v2:{base_path}/services/search/v2/jobs/{pathv1}/results?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchJobsByIdResultsPreviewWithOptions(searchId, queryData, callback)</td>
    <td style="padding:15px">getSearchJobsByIdResultsPreviewWithOptions</td>
    <td style="padding:15px">v1:{base_path}/{version}/services/search/jobs/{pathv1}/results_preview?{query} <br /> v2:{base_path}/services/search/v2/jobs/{pathv1}/results_preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchJobsSearchLogs(searchId, attachment, callback)</td>
    <td style="padding:15px">getSearchJobsSearchLogs</td>
    <td style="padding:15px">{base_path}/{version}/services/search/jobs/{pathv1}/search.log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchJobsbyIdSummary(searchId, callback)</td>
    <td style="padding:15px">getSearchJobsbyIdSummary</td>
    <td style="padding:15px">{base_path}/{version}/services/search/jobs/{pathv1}/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchJobsByIdTimeline(searchId, outputTimeFormat, timeFormat, callback)</td>
    <td style="padding:15px">getSearchJobsByIdTimeline</td>
    <td style="padding:15px">{base_path}/{version}/services/search/jobs/{pathv1}/timeline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchParser(q, callback)</td>
    <td style="padding:15px">getSearchParser</td>
    <td style="padding:15px">v1:{base_path}/{version}/services/search/parser?{query} <br /> v2:{base_path}/services/search/v2/parser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchScheduler(callback)</td>
    <td style="padding:15px">getSearchScheduler</td>
    <td style="padding:15px">{base_path}/{version}/services/search/scheduler?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSearchSchedulerStatus(body, callback)</td>
    <td style="padding:15px">postSearchSchedulerStatus</td>
    <td style="padding:15px">{base_path}/{version}/services/search/scheduler/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchTimerParser(time, callback)</td>
    <td style="padding:15px">getSearchTimerParser</td>
    <td style="padding:15px">{base_path}/{version}/services/search/timeparse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchTypeHead(count, prefix, callback)</td>
    <td style="padding:15px">getSearchTypeHead</td>
    <td style="padding:15px">{base_path}/{version}/services/search/typeahead?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertActions(count, f, offset, search, sortDir, sortKey, sortMode, summarize, callback)</td>
    <td style="padding:15px">getAlertActions</td>
    <td style="padding:15px">{base_path}/{version}/services/admin/-/alerts/alert_actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFiredAlerts(count, f, offset, search, sortDir, sortKey, sortMode, summarize, callback)</td>
    <td style="padding:15px">getFiredAlerts</td>
    <td style="padding:15px">{base_path}/{version}/services/admin/search/alerts/fired_alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFiredAlertsbyName(name, callback)</td>
    <td style="padding:15px">getFiredAlertsbyName</td>
    <td style="padding:15px">{base_path}/{version}/services/admin/search/alerts/fired_alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFiredAlertsByName(name, body, callback)</td>
    <td style="padding:15px">deleteFiredAlertsByName</td>
    <td style="padding:15px">{base_path}/{version}/services/admin/search/alerts/fired_alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataCommands(callback)</td>
    <td style="padding:15px">getDataCommands</td>
    <td style="padding:15px">{base_path}/{version}/services/nobody/search/data/commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
