# Splunk

Vendor: Cisco Systems
Homepage: https://www.cisco.com/

Product: Splunk
Product Page: https://www.splunk.com/

## Introduction
We classify Splunk into the Service Assurance domain as Splunk interacts and manages collected data.

## Why integrate
The Splunk adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Splunk to to either run searches, or manage objects and configuration. 

With this adapter you have the ability to perform operations with Splunk on items such as:

- Jobs
- Status
- Schedule
- Parser

## Additional Product Documentation
The [API documents for Splunk](https://docs.splunk.com/Documentation/Splunk/9.1.0/RESTUM/RESTusing)